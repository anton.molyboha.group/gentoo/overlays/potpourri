# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_8 python3_9 python3_10 python3_11 python3_12)
DISTUTILS_USE_PEP517=setuptools
inherit distutils-r1

DESCRIPTION="Python client for UpCloud's API"
HOMEPAGE="https://github.com/UpCloudLtd/upcloud-python-api"
SRC_URI="https://github.com/UpCloudLtd/upcloud-python-api/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

COMMON_DEPEND="
	>=dev-python/requests-2.24.0
	"
DEPEND="
	${COMMON_DEPEND}
	"
RDEPEND="${COMMON_DEPEND}"
BDEPEND=""
