# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_8 python3_9 python3_10 )
inherit distutils-r1

DESCRIPTION=""
HOMEPAGE=""
SRC_URI="https://github.com/UpCloudLtd/upcloud-python-api/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"

COMMON_DEPEND="
	dev-python/pip
	>=dev-python/urllib3-1.25.9
	>=dev-python/requests-2.24.0
	>=dev-python/six-1.15.0
	>=dev-python/python-dateutil-2.8.1
	"
DEPEND="
	${COMMON_DEPEND}
	"
RDEPEND="${COMMON_DEPEND}"
BDEPEND=""

PATCHES=( "${FILESDIR}/pytest-runner.patch" )
