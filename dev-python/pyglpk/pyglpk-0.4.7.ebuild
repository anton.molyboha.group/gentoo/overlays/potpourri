# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
DISTUTILS_EXT=1
PYTHON_COMPAT=( python3_{10,11} )

inherit distutils-r1

DESCRIPTION=""
HOMEPAGE="https://github.com/bradfordboyle/pyglpk"
SRC_URI="https://github.com/bradfordboyle/pyglpk/archive/refs/tags/v${PV}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
	sci-mathematics/glpk
"
RDEPEND="
	${DEPEND}
"
BDEPEND="
"

# PATCHES="
# 	${FILESDIR}/pyglpk-version-${PV}.patch
# "

python_compile() {
	SETUPTOOLS_SCM_PRETEND_VERSION="${PV}" distutils-r1_python_compile
}

distutils_enable_tests pytest
