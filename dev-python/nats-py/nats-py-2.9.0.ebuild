# Copyright 2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_12 )

inherit distutils-r1

DESCRIPTION="Python3 client for NATS"
HOMEPAGE="
	https://nats-io.github.io/nats.py/
"

SRC_URI="https://github.com/nats-io/nats.py/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/nats.py-${PV}"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="
"
BDEPEND="
"

distutils_enable_tests pytest
