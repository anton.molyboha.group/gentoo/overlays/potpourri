# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_8 )
inherit distutils-r1

DESCRIPTION="Start a Jupyter kernel on a remote host via ssh."
HOMEPAGE="https://pypi.org/project/remote-kernel/"
# SRC_URI="https://files.pythonhosted.org/packages/6c/b4/050613db251f3ff13b2fb108057110ed921c362a405c1853d0748ddb16b8/remote_kernel-1.1.tar.gz -> ${P}.tar.gz"
SRC_URI="https://github.com/JoostJM/remote_kernel/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND=""
# I can imagine that dev-python/jupyter_core would be enough, but I did not dig in to be sure.
RDEPEND="dev-python/paramiko
	dev-python/sshtunnel
	dev-python/jupyter"
BDEPEND=""

S="${WORKDIR}/remote_kernel-${PV}"
