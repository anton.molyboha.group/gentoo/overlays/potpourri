# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit go-module

DESCRIPTION="High-Performance server for NATS.io, the cloud and edge native messaging system."
HOMEPAGE="https://nats.io/"
SRC_URI="https://github.com/nats-io/nats-server/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz
	https://files.anton.molyboha.me/gentoo-files/net-misc/nats-server/${P}-deps.tar.xz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_compile() {
	ego build
}

src_install() {
	dobin nats-server
}
