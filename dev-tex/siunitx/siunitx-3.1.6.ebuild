# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit latex-package

DESCRIPTION="A comprehensive (SI) units package for LaTeX"
HOMEPAGE="https://www.texdev.net/"
SRC_URI="https://github.com/josephwright/siunitx/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LPPL-1.3c"
SLOT="0"
KEYWORDS="~amd64"

# It seems that the language dependencies are only needed "for demos", but I don't know an easy
# way to turn those demos off.
DEPEND="${DEPEND}
	dev-texlive/texlive-langportuguese
	dev-texlive/texlive-langother
	dev-texlive/texlive-langfrench
	dev-texlive/texlive-langgerman
	dev-texlive/texlive-langpolish
	dev-texlive/texlive-langspanish
	dev-texlive/texlive-langenglish
"
RDEPEND="${DEPEND}"
BDEPEND="dev-texlive/texlive-latex"
